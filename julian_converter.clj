#!/usr/bin/env bb
;; take the year and day as numbers and convert to a format knowing month

(defn convert-date-nums
  "takes nums like `2024034` and converts them to show the month"
  [d]
  (let [from-pattern (java.time.format.DateTimeFormatter/ofPattern "yyyyDDD")
        to-pattern (java.time.format.DateTimeFormatter/ofPattern "yyyy.MM.dd")]
    (->> (.parse from-pattern d)
         (.format to-pattern)
         str
         println)))

(defn -main
  "Main process"
  []
  (let [date-nums (first *command-line-args*)]
    (convert-date-nums date-nums)))

;;;;;;;;;;;;;;;;
;;    EXECUTE ;;
;;;;;;;;;;;;;;;;
(-main)
